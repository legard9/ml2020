#gc()

library(rpart)
library(rpart.plot)
library(RColorBrewer)
library(rattle)
library(multiROC)
library(caret)
library(ggplot2)

dataset = read.csv("DATASET/car.data", header = FALSE, sep=",")
colnames(dataset)[1] = "buying"
colnames(dataset)[2] = "maint"
colnames(dataset)[3] = "doors"
colnames(dataset)[4] = "persons"
colnames(dataset)[5] = "lug_boot"
colnames(dataset)[6] = "safety"
colnames(dataset)[7] = "class"

dataset <- dataset[, c("buying", "maint", "doors", "persons", "lug_boot", "safety", "class")]

#shuffle data
dataset<-dataset[sample(nrow(dataset)),]

folds <- cut(seq(1,nrow(dataset)),breaks=10,labels=FALSE)


ROC.sum.tree = 0

AUC.sum = 0
sumAccuracy = 0
table.sum = 0
table.stats = 0
Final.accuracy = 0


for(i in 1:10){
  #Segement your data by fold using the which() function
  testIndexes <- which(folds==i,arr.ind=TRUE)
  testset <- dataset[testIndexes, ]
  trainset <- dataset[-testIndexes, ]
  
  #rendo standard la lunghezza del testset tra tutti i fold
  if(nrow(testset) == 173){
    
    testset = head(testset,-1)
    
  }
  
  #costruzione oggetti necessari al calcolo della multiROC
  
  unacc_true = integer(nrow(testset))
  acc_true = integer(nrow(testset))
  good_true = integer(nrow(testset))
  vgood_true = integer(nrow(testset))
  
  values = cbind(unacc_true,acc_true,good_true,vgood_true)
  values = data.frame(values[,c("unacc_true","acc_true","good_true","vgood_true")])
  
  for(j in 1:nrow(testset)){
    if(testset$class[j] == "unacc"){
      values$unacc_true[j] = 1
    }else if(testset$class[j] == "acc"){
      values$acc_true[j] = 1
    }else if(testset$class[j] == "good"){
      values$good_true[j] = 1
    }else{
      values$vgood_true[j] = 1
    }
  }

  start.time <- Sys.time()
  
  tree.model = rpart(class ~ buying + maint + doors + persons + lug_boot + safety,
               data=trainset,
               method="class",
               parms = list(split = 'information'))

  end.time <- Sys.time()
  time.taken <- end.time - start.time
  time.taken
  cat("time: ", time.taken,"\n")

  predicted.prob.tree = predict(tree.model, testset, type = "prob")
  predicted.class.tree = predict(tree.model, testset, type = "class")

  predicted.prob.tree = predicted.prob.tree[,c("unacc","acc","good","vgood")]

  colnames(predicted.prob.tree)[1] = "unacc_pred_tree"
  colnames(predicted.prob.tree)[2] = "acc_pred_tree"
  colnames(predicted.prob.tree)[3] = "good_pred_tree"
  colnames(predicted.prob.tree)[4] = "vgood_pred_tree"

  data.ROC.tree = cbind(values,predicted.prob.tree)

  ROC.tree = multi_roc(data.ROC.tree,force_diag = TRUE)
  
  confmat = confusionMatrix(predicted.class.tree,testset$class, mode = "prec_recall")
  
  if(i == 1){
    
    ROC.sum.tree = ROC.tree
    
  }else{
    
    #Somma specificita
    ROC.sum.tree$Specificity$tree$unacc = Map("+",
                                              ROC.sum.tree$Specificity$tree$unacc,
                                              ROC.tree$Specificity$tree$unacc)
    ROC.sum.tree$Specificity$tree$acc = Map("+",
                                              ROC.sum.tree$Specificity$tree$acc,
                                              ROC.tree$Specificity$tree$acc)
    ROC.sum.tree$Specificity$tree$good = Map("+",
                                              ROC.sum.tree$Specificity$tree$good,
                                              ROC.tree$Specificity$tree$good)
    ROC.sum.tree$Specificity$tree$vgood = Map("+",
                                              ROC.sum.tree$Specificity$tree$vgood,
                                              ROC.tree$Specificity$tree$vgood)
    ROC.sum.tree$Specificity$tree$macro = Map("+",
                                              ROC.sum.tree$Specificity$tree$macro,
                                              ROC.tree$Specificity$tree$macro)
    ROC.sum.tree$Specificity$tree$micro = Map("+",
                                              ROC.sum.tree$Specificity$tree$micro,
                                              ROC.tree$Specificity$tree$micro)
    
    #Somma sensitivita
    ROC.sum.tree$Sensitivity$tree$unacc = Map("+",
                                              ROC.sum.tree$Sensitivity$tree$unacc,
                                              ROC.tree$Sensitivity$tree$unacc)
    ROC.sum.tree$Sensitivity$tree$acc = Map("+",
                                            ROC.sum.tree$Sensitivity$tree$acc,
                                            ROC.tree$Sensitivity$tree$acc)
    ROC.sum.tree$Sensitivity$tree$good = Map("+",
                                             ROC.sum.tree$Sensitivity$tree$good,
                                             ROC.tree$Sensitivity$tree$good)
    ROC.sum.tree$Sensitivity$tree$vgood = Map("+",
                                              ROC.sum.tree$Sensitivity$tree$vgood,
                                              ROC.tree$Sensitivity$tree$vgood)
    ROC.sum.tree$Sensitivity$tree$macro = Map("+",
                                              ROC.sum.tree$Sensitivity$tree$macro,
                                              ROC.tree$Sensitivity$tree$macro)
    ROC.sum.tree$Sensitivity$tree$micro = Map("+",
                                              ROC.sum.tree$Sensitivity$tree$micro,
                                              ROC.tree$Sensitivity$tree$micro)
    
    ROC.sum.tree$AUC$tree$unacc = ROC.sum.tree$AUC$tree$unacc + ROC.tree$AUC$tree$unacc
    ROC.sum.tree$AUC$tree$acc = ROC.sum.tree$AUC$tree$acc + ROC.tree$AUC$tree$acc
    ROC.sum.tree$AUC$tree$good = ROC.sum.tree$AUC$tree$good + ROC.tree$AUC$tree$good
    ROC.sum.tree$AUC$tree$vgood = ROC.sum.tree$AUC$tree$vgood + ROC.tree$AUC$tree$vgood
    ROC.sum.tree$AUC$tree$macro = ROC.sum.tree$AUC$tree$macro + ROC.tree$AUC$tree$macro
    ROC.sum.tree$AUC$tree$micro = ROC.sum.tree$AUC$tree$micro + ROC.tree$AUC$tree$micro
    
    
  }
  
  #Estraggo i dati necessari al calcolo delle statistiche dalla matrice di confusione
  table.sum = table.sum + confmat$table
  Accuracy = confmat$overall[1]
  table.stats = table.stats + confmat$byClass
  
  sumAccuracy = sumAccuracy + Accuracy
}

Final.accuracy = sumAccuracy /10
cat("Accuracy: ",Final.accuracy,"\n")

#Calcolo statistiche medie per ogni classe
for(i in 1:4){
  for(j in 1:11){
    table.stats[i,j] = table.stats[i,j]/10
  }
}

#calcolo roc tree media
ROC.avg = ROC.tree

for(i in 1:length(ROC.avg$Specificity$tree$unacc)){
  
    ROC.avg$Specificity$tree$unacc[[i]] = ROC.sum.tree$Specificity$tree$unacc[[i]] / 10
    ROC.avg$Specificity$tree$acc[[i]] = ROC.sum.tree$Specificity$tree$acc[[i]] / 10
    ROC.avg$Specificity$tree$good[[i]] = ROC.sum.tree$Specificity$tree$good[[i]] / 10
    ROC.avg$Specificity$tree$vgood[[i]] = ROC.sum.tree$Specificity$tree$vgood[[i]] / 10
    
    ROC.avg$Sensitivity$tree$unacc[[i]] = ROC.sum.tree$Sensitivity$tree$unacc[[i]] / 10
    ROC.avg$Sensitivity$tree$acc[[i]] = ROC.sum.tree$Sensitivity$tree$acc[[i]] / 10
    ROC.avg$Sensitivity$tree$good[[i]] = ROC.sum.tree$Sensitivity$tree$good[[i]] / 10
    ROC.avg$Sensitivity$tree$vgood[[i]] = ROC.sum.tree$Sensitivity$tree$vgood[[i]] / 10
}

for(i in 1:length(ROC.avg$Specificity$tree$macro)){
  
  ROC.avg$Specificity$tree$macro[[i]] = ROC.sum.tree$Specificity$tree$macro[[i]] / 10

  ROC.avg$Sensitivity$tree$macro[[i]] = ROC.sum.tree$Sensitivity$tree$macro[[i]] / 10
}

for(i in 1:length(ROC.avg$Specificity$tree$micro)){
  
  ROC.avg$Specificity$tree$micro[[i]] = ROC.sum.tree$Specificity$tree$micro[[i]] / 10
  
  ROC.avg$Sensitivity$tree$micro[[i]] = ROC.sum.tree$Sensitivity$tree$micro[[i]] / 10
}

ROC.avg$AUC$tree$unacc = ROC.sum.tree$AUC$tree$unacc /10
ROC.avg$AUC$tree$acc = ROC.sum.tree$AUC$tree$acc /10
ROC.avg$AUC$tree$good = ROC.sum.tree$AUC$tree$good /10
ROC.avg$AUC$tree$vgood = ROC.sum.tree$AUC$tree$vgood /10
ROC.avg$AUC$tree$macro = ROC.sum.tree$AUC$tree$macro /10
ROC.avg$AUC$tree$micro = ROC.sum.tree$AUC$tree$micro /10


res = plot_roc_data(ROC.avg)


r = 0

for(i in 1:nrow(res)){
  
  if(res$Group[i] == "Macro" || res$Group[i] == "Micro"){
    r = i
    break
  }
  
}

del = nrow(res) - i +1

cleaned = head(res,-del)


n_method <- length(unique(cleaned$Method))
n_group <- length(unique(cleaned$Group))

plot(ggplot(cleaned, aes(x = 1-Specificity, y=Sensitivity)) + geom_path(aes(color = Group, linetype=Method)) + geom_segment(ggplot2::aes(x = 0, y = 0, xend = 1, yend = 1), colour='grey', linetype = 'dotdash') + theme_bw() + theme(plot.title = element_text(hjust = 0.5), legend.justification=c(1, 0), legend.position=c(.95, .05), legend.title=element_blank(), legend.background = element_rect(fill=NULL, size=0.5, linetype="solid", colour ="black")))
