library(e1071)
library(caret)
library(ROCR)

dataset = read.csv("DATASET/car2.data.csv", header = TRUE, sep=",")

colnames(dataset)[1] = "buying"
colnames(dataset)[2] = "maint"
colnames(dataset)[3] = "doors"
colnames(dataset)[4] = "persons"
colnames(dataset)[5] = "lug_boot"
colnames(dataset)[6] = "safety"
colnames(dataset)[7] = "class"

#Unisco le classi per ottenere classificazione binaria
dataset.unacc = dataset[dataset$class == "unacc",]
dataset.acc = rbind(dataset[dataset$class == "acc",],dataset[dataset$class == "good",], dataset[dataset$class == "vgood",])

dataset.unacc$class = "unacc"
dataset.acc$class = "acc"

dataset.unacc$class = as.factor(dataset.unacc$class)
dataset.acc$class = as.factor(dataset.acc$class)


dataset = rbind(dataset.unacc,dataset.acc)

#shuffle data
dataset<-dataset[sample(nrow(dataset)),]

folds <- cut(seq(1,nrow(dataset)),breaks=10,labels=FALSE)

sum = 0
AUC.sum = 0
sumAccuracy = 0
sumPrecision = 0
sumRecall = 0
sumF1 = 0

sum.tpr = 0
table.sum = 0
start.time <- Sys.time()
#Perform 10 fold cross validation
for(i in 1:10){
  #Creo trainset e testset per il fold
  testIndexes <- which(folds==i,arr.ind=TRUE)
  testset <- dataset[testIndexes, ]
  trainset <- dataset[-testIndexes, ]
  
  #Training
  svm.model = svm(trainset$class ~., data=trainset, kernel = "linear",probability = TRUE)

  values = testset$class
  testset$class = NULL
  
  #Prediction
  svm.pred = predict(svm.model,testset,probability = TRUE)
  
  #Estraggo probabilit? di acc per curva ROC
  svm.probs = attr(svm.pred,"probabilities")
  
  acc.prob = svm.probs[,1]
  
  #qualcosa non va
  pred.rocr = prediction(acc.prob,values)
  

  svm.perf = performance(pred.rocr,measure="auc",x.measure = "cutoff") #calcolare media
  perf.tpr = performance(pred.rocr,"tpr","fpr")

  AUC.sum = AUC.sum + svm.perf@y.values[[1]]
  
  #Plot della curva
  if(i==1){
    sum.tpr = perf.tpr
    
    if(length(sum.tpr@x.values[[1]]) == 174){
      
          
      sum.tpr@x.values[[1]] = head(sum.tpr@x.values[[1]],-1)
      sum.tpr@y.values[[1]] = head(sum.tpr@y.values[[1]],-1)
      sum.tpr@alpha.values[[1]] = head(sum.tpr@alpha.values[[1]],-1)
      
    }
    
    
    
    plot(perf.tpr,colorize = TRUE)
    
  }else{
    
    tmp.tpr = perf.tpr
    
    
    if(length(tmp.tpr@x.values[[1]]) == 174){

      
      tmp.tpr@x.values[[1]] = head(tmp.tpr@x.values[[1]],-1)
      tmp.tpr@y.values[[1]] = head(tmp.tpr@y.values[[1]],-1)
      tmp.tpr@alpha.values[[1]] = head(tmp.tpr@alpha.values[[1]],-1)
      
      
    }
    
    sum.tpr@x.values = Map("+",tmp.tpr@x.values,sum.tpr@x.values)
    sum.tpr@y.values = Map("+",tmp.tpr@y.values,sum.tpr@y.values)
    sum.tpr@alpha.values = Map("+",tmp.tpr@alpha.values,sum.tpr@alpha.values)
    
    plot(perf.tpr,add=TRUE,colorize = TRUE)
  }

  abline(a=0,b=1)

  #confusion matrix e statistiche varie
  confmat = confusionMatrix(svm.pred,values, mode = "prec_recall")
  
  table.sum = table.sum + confmat$table
  Accuracy = confmat[["overall"]][["Accuracy"]]
  Precision = confmat[["byClass"]][["Precision"]]
  Recall = confmat[["byClass"]][["Recall"]]
  F1 = confmat[["byClass"]][["F1"]]

  sumAccuracy = sumAccuracy + Accuracy
  sumPrecision = sumPrecision + Precision
  sumRecall = sumRecall + Recall
  sumF1 = sumF1 + F1
}

avg.tpr = sum.tpr

for(i in 1:173){
  avg.tpr@x.values[[1]][i] = sum.tpr@x.values[[1]][i]/10
  avg.tpr@y.values[[1]][i] = sum.tpr@y.values[[1]][i]/10
  avg.tpr@alpha.values[[1]][i] = sum.tpr@alpha.values[[1]][i]/10
}

plot(avg.tpr,add = TRUE,col = "blue")

final.AUC = AUC.sum/10

title(paste("AUC: ",final.AUC))

Final.Accuracy = sumAccuracy/10
Final.Precision = sumPrecision/10
Final.Recall = sumRecall/10
Final.F1 = sumF1/10

cat("Accuracy: ",Final.Accuracy,"\n")
cat("Precision: ",Final.Precision,"\n")
cat("Recall: ",Final.Recall,"\n")
cat("F1: ",Final.F1,"\n")
end.time <- Sys.time()
time.taken <- end.time - start.time
time.taken
cat("time: ", time.taken,"\n")