library("FactoMineR")
library("factoextra")

dataset = read.csv("DATASET/car2.data.csv", header = FALSE, sep=",")

colnames(dataset)[1] = "buying"
colnames(dataset)[2] = "maint"
colnames(dataset)[3] = "doors"
colnames(dataset)[4] = "persons"
colnames(dataset)[5] = "lug_boot"
colnames(dataset)[6] = "safety"
colnames(dataset)[7] = "class"

dataset <- dataset[sample(nrow(dataset)),]


dataset <- dataset[1:1552, c(1:6)]
data.pca <- PCA(dataset, graph = TRUE)
eigen = get_eigenvalue(data.pca)
plot(fviz_pca_var(data.pca,
                  col.var = "contrib", # Color by contributions to the PC
                  gradient.cols = c("#00AFBB", "#E7B800", "#FC4E07"),
                  repel = TRUE     # Avoid text overlapping
))

