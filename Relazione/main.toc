\contentsline {section}{\numberline {1}Introduzione}{2}{section.1}
\contentsline {section}{\numberline {2}Dataset}{2}{section.2}
\contentsline {subsection}{\numberline {2.1}Attributi}{2}{subsection.2.1}
\contentsline {subsubsection}{\numberline {2.1.1}Elaborazioni degli Attributi}{2}{subsubsection.2.1.1}
\contentsline {subsection}{\numberline {2.2}Classe Target}{2}{subsection.2.2}
\contentsline {subsubsection}{\numberline {2.2.1}Elaborazione della Classe Target}{3}{subsubsection.2.2.1}
\contentsline {section}{\numberline {3}Analisi Trainset}{3}{section.3}
\contentsline {subsection}{\numberline {3.1}Problema a 4 Classi}{3}{subsection.3.1}
\contentsline {subsubsection}{\numberline {3.1.1}Analisi PCA}{4}{subsubsection.3.1.1}
\contentsline {subsection}{\numberline {3.2}Problema Binario}{4}{subsection.3.2}
\contentsline {section}{\numberline {4}Modelli Utilizzati}{4}{section.4}
\contentsline {subsection}{\numberline {4.1}Problema a 4 Classi}{4}{subsection.4.1}
\contentsline {subsubsection}{\numberline {4.1.1}Alberi Decisionali}{4}{subsubsection.4.1.1}
\contentsline {paragraph}{}{5}{section*.2}
\contentsline {subsubsection}{\numberline {4.1.2}Rete Neurale}{5}{subsubsection.4.1.2}
\contentsline {paragraph}{}{5}{section*.3}
\contentsline {subsection}{\numberline {4.2}Problema Binario}{6}{subsection.4.2}
\contentsline {subsubsection}{\numberline {4.2.1}Support Vector Machine}{6}{subsubsection.4.2.1}
\contentsline {paragraph}{}{6}{section*.4}
\contentsline {subsubsection}{\numberline {4.2.2}Rete Neurale}{6}{subsubsection.4.2.2}
\contentsline {section}{\numberline {5}Risultati degli Esperimenti}{6}{section.5}
\contentsline {subsection}{\numberline {5.1}Problema a 4 Classi}{7}{subsection.5.1}
\contentsline {subsubsection}{\numberline {5.1.1}Albero Decisionale}{7}{subsubsection.5.1.1}
\contentsline {subsubsection}{\numberline {5.1.2}Rete Neurale}{7}{subsubsection.5.1.2}
\contentsline {subsection}{\numberline {5.2}Problema Binario}{8}{subsection.5.2}
\contentsline {subsubsection}{\numberline {5.2.1}SVM}{8}{subsubsection.5.2.1}
\contentsline {subsubsection}{\numberline {5.2.2}Rete Neurale}{8}{subsubsection.5.2.2}
\contentsline {section}{\numberline {6}Analisi Risultati}{9}{section.6}
\contentsline {subsection}{\numberline {6.1}Problema a 4 Classi}{9}{subsection.6.1}
\contentsline {subsection}{\numberline {6.2}Problema Binario}{9}{subsection.6.2}
\contentsline {section}{\numberline {7}Conclusioni}{9}{section.7}
