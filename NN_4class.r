library(neuralnet)
library(ggplot2)
library(multiROC)
library(caret)

#---------------------------------------------------------
#SCRIPT

dataset = read.csv("DATASET/car2.data.csv", header = FALSE, sep=",")

colnames(dataset)[1] = "buying"
colnames(dataset)[2] = "maint"
colnames(dataset)[3] = "doors"
colnames(dataset)[4] = "persons"
colnames(dataset)[5] = "lug_boot"
colnames(dataset)[6] = "safety"
colnames(dataset)[7] = "class"

#shuffle data
dataset<-dataset[sample(nrow(dataset)),]

folds <- cut(seq(1,nrow(dataset)),breaks=10,labels=FALSE)

start.time <- Sys.time()

sum = 0

ROC.sum.nn = 0

AUC.sum = 0
sumAccuracy = 0
table.sum = 0
table.stats = 0
Final.accuracy = 0

#Perform 10 fold cross validation
for(i in 1:10){
  #Segment your data by fold using the which() function
  testIndexes <- which(folds==i,arr.ind=TRUE)
  testset <- dataset[testIndexes, ]
  trainset <- dataset[-testIndexes, ]
  
  #rendo standard la lunghezza del testset tra tutti i fold
  if(nrow(testset) == 173){
    
    testset = head(testset,-1)
    
  }
  
  #costruzione oggetti necessari al calcolo della multiROC
  
  unacc_true = integer(nrow(testset))
  acc_true = integer(nrow(testset))
  good_true = integer(nrow(testset))
  vgood_true = integer(nrow(testset))
  
  values = cbind(unacc_true,acc_true,good_true,vgood_true)
  values = data.frame(values[,c("unacc_true","acc_true","good_true","vgood_true")])
  
  for(j in 1:nrow(testset)){
    if(testset$class[j] == "unacc"){
      values$unacc_true[j] = 1
    }else if(testset$class[j] == "acc"){
      values$acc_true[j] = 1
    }else if(testset$class[j] == "good"){
      values$good_true[j] = 1
    }else{
      values$vgood_true[j] = 1
    }
  }
  
  
  trainset$unacc = trainset$class == "unacc"
  trainset$acc = trainset$class == "acc" 
  trainset$good = trainset$class == "good"
  trainset$vgood = trainset$class == "vgood"
  
  testset.true = testset$class
  #testset$class = NULL
  
  
  network = neuralnet(unacc + acc + good + vgood~ buying + maint + doors + persons + lug_boot + safety,
                      trainset, hidden=13, stepmax = 1e7, rep = 1)

  
  predicted.prob.nn = compute(network, testset[-7])$net.result
  predicted.class.nn = c("unacc", "acc", "good", "vgood")[apply(predicted.prob.nn, 1,
                                                            which.max)]
  predict.table = table(testset.true, predicted.class.nn)
  
  
  data.ROC.nn = cbind(values,predicted.prob.nn)
  
  colnames(data.ROC.nn)[5] = "unacc_pred_nn"
  colnames(data.ROC.nn)[6] = "acc_pred_nn"
  colnames(data.ROC.nn)[7] = "good_pred_nn"
  colnames(data.ROC.nn)[8] = "vgood_pred_nn"

  ROC.nn = multi_roc(data.ROC.nn,force_diag = TRUE)
  
  confmat = confusionMatrix(factor(predicted.class.nn),testset$class, mode = "prec_recall")
  
  
  
  if(i == 1){
    
    ROC.sum.nn = ROC.nn
    
  }else{
    
    #Somma specificita
    ROC.sum.nn$Specificity$nn$unacc = Map("+",
                                            ROC.sum.nn$Specificity$nn$unacc,
                                          ROC.nn$Specificity$nn$unacc)
    ROC.sum.nn$Specificity$nn$acc = Map("+",
                                          ROC.sum.nn$Specificity$nn$acc,
                                        ROC.nn$Specificity$nn$acc)
    ROC.sum.nn$Specificity$nn$good = Map("+",
                                           ROC.sum.nn$Specificity$nn$good,
                                         ROC.nn$Specificity$nn$good)
    ROC.sum.nn$Specificity$nn$vgood = Map("+",
                                            ROC.sum.nn$Specificity$nn$vgood,
                                          ROC.nn$Specificity$nn$vgood)
    ROC.sum.nn$Specificity$nn$macro = Map("+",
                                            ROC.sum.nn$Specificity$nn$macro,
                                          ROC.nn$Specificity$nn$macro)
    ROC.sum.nn$Specificity$nn$micro = Map("+",
                                            ROC.sum.nn$Specificity$nn$micro,
                                          ROC.nn$Specificity$nn$micro)
    
    #Somma sensitivita
    ROC.sum.nn$Sensitivity$nn$unacc = Map("+",
                                          ROC.sum.nn$Sensitivity$nn$unacc,
                                          ROC.nn$Sensitivity$nn$unacc)
    ROC.sum.nn$Sensitivity$nn$acc = Map("+",
                                        ROC.sum.nn$Sensitivity$nn$acc,
                                        ROC.nn$Sensitivity$nn$acc)
    ROC.sum.nn$Sensitivity$nn$good = Map("+",
                                         ROC.sum.nn$Sensitivity$nn$good,
                                         ROC.nn$Sensitivity$nn$good)
    ROC.sum.nn$Sensitivity$nn$vgood = Map("+",
                                          ROC.sum.nn$Sensitivity$nn$vgood,
                                          ROC.nn$Sensitivity$nn$vgood)
    ROC.sum.nn$Sensitivity$nn$macro = Map("+",
                                            ROC.sum.nn$Sensitivity$nn$macro,
                                            ROC.nn$Sensitivity$nn$macro)
    ROC.sum.nn$Sensitivity$nn$micro = Map("+",
                                            ROC.sum.nn$Sensitivity$nn$micro,
                                            ROC.nn$Sensitivity$nn$micro)
    
    ROC.sum.nn$AUC$nn$unacc = ROC.sum.nn$AUC$nn$unacc + ROC.nn$AUC$nn$unacc
    ROC.sum.nn$AUC$nn$acc = ROC.sum.nn$AUC$nn$acc + ROC.nn$AUC$nn$acc
    ROC.sum.nn$AUC$nn$good = ROC.sum.nn$AUC$nn$good + ROC.nn$AUC$nn$good
    ROC.sum.nn$AUC$nn$vgood = ROC.sum.nn$AUC$nn$vgood + ROC.nn$AUC$nn$vgood
    ROC.sum.nn$AUC$nn$macro = ROC.sum.nn$AUC$nn$macro + ROC.nn$AUC$nn$macro
    ROC.sum.nn$AUC$nn$micro = ROC.sum.nn$AUC$nn$micro + ROC.nn$AUC$nn$micro
    
    
  }
  
  
  #Estraggo i dati necessari al calcolo delle statistiche dalla matrice di confusione
  table.sum = table.sum + confmat$table
  Accuracy = confmat$overall[1]
  table.stats = table.stats + confmat$byClass
  
  sumAccuracy = sumAccuracy + Accuracy
  
  cat("acc: ", Accuracy, "\n")
}


end.time <- Sys.time()
time.taken <- end.time - start.time
time.taken
cat("time: ", time.taken)


Final.accuracy = sumAccuracy /10
cat("Accuracy: ",Final.accuracy,"\n")

#Calcolo statistiche medie per ogni classe
for(i in 1:4){
  for(j in 1:11){
    table.stats[i,j] = table.stats[i,j]/10
  }
}

ROC.avg = ROC.nn

for(i in 1:length(ROC.avg$Specificity$nn$unacc)){
  
  ROC.avg$Specificity$nn$unacc[[i]] = ROC.sum.nn$Specificity$nn$unacc[[i]] / 10
  ROC.avg$Specificity$nn$acc[[i]] = ROC.sum.nn$Specificity$nn$acc[[i]] / 10
  ROC.avg$Specificity$nn$good[[i]] = ROC.sum.nn$Specificity$nn$good[[i]] / 10
  ROC.avg$Specificity$nn$vgood[[i]] = ROC.sum.nn$Specificity$nn$vgood[[i]] / 10
  
  ROC.avg$Sensitivity$nn$unacc[[i]] = ROC.sum.nn$Sensitivity$nn$unacc[[i]] / 10
  ROC.avg$Sensitivity$nn$acc[[i]] = ROC.sum.nn$Sensitivity$nn$acc[[i]] / 10
  ROC.avg$Sensitivity$nn$good[[i]] = ROC.sum.nn$Sensitivity$nn$good[[i]] / 10
  ROC.avg$Sensitivity$nn$vgood[[i]] = ROC.sum.nn$Sensitivity$nn$vgood[[i]] / 10
}

for(i in 1:length(ROC.avg$Specificity$nn$macro)){
  
  ROC.avg$Specificity$nn$macro[[i]] = ROC.sum.nn$Specificity$nn$macro[[i]] / 10
  
  ROC.avg$Sensitivity$nn$macro[[i]] = ROC.sum.nn$Sensitivity$nn$macro[[i]] / 10
}

for(i in 1:length(ROC.avg$Specificity$nn$micro)){
  
  ROC.avg$Specificity$nn$micro[[i]] = ROC.sum.nn$Specificity$nn$micro[[i]] / 10
  
  ROC.avg$Sensitivity$nn$micro[[i]] = ROC.sum.nn$Sensitivity$nn$micro[[i]] / 10
}

ROC.avg$AUC$nn$unacc = ROC.sum.nn$AUC$nn$unacc /10
ROC.avg$AUC$nn$acc = ROC.sum.nn$AUC$nn$acc /10
ROC.avg$AUC$nn$good = ROC.sum.nn$AUC$nn$good /10
ROC.avg$AUC$nn$vgood = ROC.sum.nn$AUC$nn$vgood /10
ROC.avg$AUC$nn$macro = ROC.sum.nn$AUC$nn$macro /10
ROC.avg$AUC$nn$micro = ROC.sum.nn$AUC$nn$micro /10


res = plot_roc_data(ROC.avg)


r = 0

for(i in 1:nrow(res)){
  
  if(res$Group[i] == "Macro" || res$Group[i] == "Micro"){
    r = i
    break
  }
  
}

del = nrow(res) - i +1

cleaned = head(res,-del)


n_method <- length(unique(cleaned$Method))
n_group <- length(unique(cleaned$Group))

plot(ggplot(cleaned, aes(x = 1-Specificity, y=Sensitivity)) + geom_path(aes(color = Group, linetype=Method)) + geom_segment(ggplot2::aes(x = 0, y = 0, xend = 1, yend = 1), colour='grey', linetype = 'dotdash') + theme_bw() + theme(plot.title = element_text(hjust = 0.5), legend.justification=c(1, 0), legend.position=c(.95, .05), legend.title=element_blank(), legend.background = element_rect(fill=NULL, size=0.5, linetype="solid", colour ="black")))

plot(network)





