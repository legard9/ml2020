library(neuralnet)
library(caret)
library(ROCR)

#---------------------------------------------------------
#SCRIPT

dataset = read.csv("DATASET/car2.data.csv", header = FALSE, sep=",")

colnames(dataset)[1] = "buying"
colnames(dataset)[2] = "maint"
colnames(dataset)[3] = "doors"
colnames(dataset)[4] = "persons"
colnames(dataset)[5] = "lug_boot"
colnames(dataset)[6] = "safety"
colnames(dataset)[7] = "class"

dataset.unacc = dataset[dataset$class == "unacc",]
dataset.acc = rbind(dataset[dataset$class == "acc",],dataset[dataset$class == "good",], dataset[dataset$class == "vgood",])

dataset.unacc$class = "unacc"
dataset.acc$class = "acc"

dataset.unacc$class = as.factor(dataset.unacc$class)
dataset.acc$class = as.factor(dataset.acc$class)

dataset = rbind(dataset.unacc,dataset.acc)


#shuffle data
dataset<-dataset[sample(nrow(dataset)),]

folds <- cut(seq(1,nrow(dataset)),breaks=10,labels=FALSE)

start.time <- Sys.time()

sum = 0

sum.tpr = 0

AUC.sum = 0
sumAccuracy = 0
sumPrecision = 0
sumRecall = 0
sumF1 = 0
table.sum = 0

#Perform 10 fold cross validation
for(i in 1:10){
  #Segment your data by fold using the which() function
  testIndexes <- which(folds==i,arr.ind=TRUE)
  testset <- dataset[testIndexes, ]
  trainset <- dataset[-testIndexes, ]
  
  trainset$unacc = trainset$class == "unacc"
  trainset$acc = trainset$class == "acc"
  
  
  network = neuralnet(unacc + acc ~ buying + maint + doors + persons + lug_boot + safety,
                      trainset, hidden=14, stepmax = 1e7, rep = 1)
  #plot(network)
  net.predict = compute(network, testset[-7])$net.result
  net.prediction = c("unacc", "acc")[apply(net.predict, 1, which.max)]
  predict.table = table(testset$class, net.prediction)
  
  net.predict = predict(network,testset,probability = TRUE)
  
  values = testset$class
  testset$class = NULL
  
  #Estraggo probabilità di acc per curva ROC
  
  #-------------------- INIZIO CALCOLO ROC-----------------------------------
  
  acc.prob = net.predict[,2]
  
  pred.rocr = prediction(acc.prob,values,label.ordering = c("unacc","acc"))
  
  nn.perf = performance(pred.rocr,measure="auc",x.measure = "cutoff") #calcolare media
  perf.tpr = performance(pred.rocr,"tpr","fpr")
  
  AUC.sum = AUC.sum + nn.perf@y.values[[1]]
  
  #Plot della curva
  if(i==1){
    sum.tpr = perf.tpr
    
    if(length(sum.tpr@x.values[[1]]) == 174){
      
      sum.tpr@x.values[[1]] = head(sum.tpr@x.values[[1]],-1)
      sum.tpr@y.values[[1]] = head(sum.tpr@y.values[[1]],-1)
      sum.tpr@alpha.values[[1]] = head(sum.tpr@alpha.values[[1]],-1)
      
    }
    
    
    
    plot(perf.tpr,colorize = TRUE)
    
  }else{
    
    tmp.tpr = perf.tpr
    
    if(length(tmp.tpr@x.values[[1]]) == 174){
      

      tmp.tpr@x.values[[1]] = head(tmp.tpr@x.values[[1]],-1)
      tmp.tpr@y.values[[1]] = head(tmp.tpr@y.values[[1]],-1)
      tmp.tpr@alpha.values[[1]] = head(tmp.tpr@alpha.values[[1]],-1)
      
      
    }
    
    sum.tpr@x.values = Map("+",tmp.tpr@x.values,sum.tpr@x.values)
    sum.tpr@y.values = Map("+",tmp.tpr@y.values,sum.tpr@y.values)
    sum.tpr@alpha.values = Map("+",tmp.tpr@alpha.values,sum.tpr@alpha.values)
    
    plot(perf.tpr,add=TRUE,colorize = TRUE)
  }
  
  
  abline(a=0,b=1)
  
  #----------------------FINE CALCOLO ROC-------------------------
  
  #Calcoli per matrice di confusione e statistiche correlate
  
  net.prediction = factor(net.prediction)
  
  confmat = confusionMatrix(net.prediction,values, mode = "prec_recall",positive = "acc")
  
  table.sum = table.sum + table(net.prediction,values)
  
  Accuracy = confmat$overall[1]
  Precision = confmat$byClass[5]
  Recall = confmat$byClass[6]
  F1 = confmat$byClass[7]
  
  sumAccuracy = sumAccuracy + Accuracy
  sumPrecision = sumPrecision + Precision
  sumRecall = sumRecall + Recall
  sumF1 = sumF1 + F1
  
  cat("acc: ", Accuracy, "\n")
}

avg.tpr = sum.tpr

for(i in 1:173){
  avg.tpr@x.values[[1]][i] = sum.tpr@x.values[[1]][i]/10
  avg.tpr@y.values[[1]][i] = sum.tpr@y.values[[1]][i]/10
  avg.tpr@alpha.values[[1]][i] = sum.tpr@alpha.values[[1]][i]/10
}

plot(avg.tpr,add = TRUE,col = "blue")

final.AUC = AUC.sum/10

title(paste("AUC: ",final.AUC))

end.time <- Sys.time()
time.taken <- end.time - start.time
time.taken
cat("time: ", time.taken,"\n")

Final.Accuracy = sumAccuracy/10
Final.Precision = sumPrecision/10
Final.Recall = sumRecall/10
Final.F1 = sumF1/10

cat("Accuracy: ",Final.Accuracy,"\n")
cat("Precision: ",Final.Precision,"\n")
cat("Recall: ",Final.Recall,"\n")
cat("F1: ",Final.F1,"\n")
